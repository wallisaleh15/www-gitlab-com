---
layout: markdown_page
title: "Deployment Direction"
description: "The job of deploying software is a critical step in DevOps. This page highlights GitLab's direction."
canonical_path: "/direction/deployment/"
---

- TOC
{:toc}

We would appreciate your feedback on this direction page. Please comment on our [async AMA issue](https://gitlab.com/gitlab-com/Product/-/issues/2668), [email Kevin Chu](mailto:kchu@gitlab.com) or [propose an MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/source/direction/deployment/index.html.md.erb) to this page!

<%= devops_diagram(["Configure","Release"]) %>

## Overview
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/G-ssIiuwg5E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### What is Deployment?

Deployment is the step which brings coded, integrated, and built software application to a runtime environment. Once an application is deployed, users can start deriving value from the application. Deployment is part of GitLab's [Ops](/direction/ops/) section.

Deployments are challenging because:
* Deployment is simple when it's done by a single developer or a single development team working on a single project. However, applications at organizations are built by multiple teams deploying potentially thousands of components and services. This requires coordination, collaboration, orchestration, and automation.
* Organizations want to outcompete competition by deploying more frequently in order to capture value sooner. The increasing frequency compounds the stress on teams responsible for deployments. Organizations need a fast, repeatable, and safe way to deploy over and over again.
* The number of different deployment targets has also been expanding. Deployment used to be about copying new files to a server. Deployment targets can now span your own development servers, cloud infrastructure, and cloud-native container runtimes. Large organizations often have many flavors of deployment targets, contributing to the complexity in deployments. Organizations need a consistent interface for deploying to different targets.

As the runtime technology continues to advance, one side effect of this evolution is that the line between the configuration of infrastructure and the release of the software has been blurred. In order to clearly communicate our vision and strategy for these stages, GitLab's deployment direction is inclusive of both the [Configure](/direction/configure/) stage and the [Release](/direction/release/) stage to account for the advancement in deployment practices. 

### GitLab's Deployment Vision

GitLab's Deployment vision is to enable deployment to be fast, easy, and reliable, yet flexible enough to support the scale, speed, and operating model for your business.

We want you to spend the majority of your time creating new software experiences for your users instead of investing time and effort in figuring out how to deploy your software. No matter if you are an individual developer trying to ship your code to a production-ready environment or are building a platform-as-a-service to enable your organization's developers, GitLab is your one-stop-deployment-shop.


### Market
The total addressable market (TAMkt) for DevOps tools targeting the Release and Configure stages was [$1.79B in 2020 and is expected to grow to $3.25B by 2024 (13.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035). This analysis is considered conservative as it focuses only on developers and doesn't include other users. External market sizing shows even more potential. For example, for continous delivery alone, which does not include categories such as infrastructure as code, is estimated to have a [market size of $1.62B in 2018 growing to $6B by 2026 (17.76% CAGR)](https://www.verifiedmarketresearch.com/product/continuous-delivery-market/). This, like the [Ops market](/direction/ops/#market) in general, is a deep value pool and represents a significant portion of GitLab's expanding addressable market. 

The deployment tooling market is evolving and expanding. There are now many more options than just adding a delivery job to your CI/CD pipeline. Release Orchestration, advanced deployments, GitOps, infrastructure provisioning, platform as a service, progressive delivery, and feature flags are all methodologies that help teams deploy more easily, frequently, and confidently. Completeness of feature set from vendors is becoming increasingly important as teams want the benefits from all worlds; traditional, table stakes deployment features alongside modern, differentiating features.

To increase deployment frequency and be competitive in the market, enterprises have turned to [centralized cloud teams or cloud center of excellence](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526578502) that are responsible for helping [development teams be more efficient and productive](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526579050). These teams have centralized buying power for DevOps and deployment tools. They may also have organizational support to build a DIY DevOps platform. For cloud-native DIY platforms, we've found (through customer interviews) that open-source point deployment solutions (such as [Flux](https://fluxcd.io/) or [ArgoCD](https://argoproj.github.io/argo-cd/)) are the primary options because of their focus on cloud-native principles and early adoption of pull-based deployment. Otherwise, enterprises, even existing GitLab customers, sometimes buy commercial tools for deployment, such as octopus, harness, CloudBees electric flow.
  
### Current Position
The CI/CD pipeline is one of GitLab's most widely used features. Many organizations that have adopted the GitLab pipeline also use GitLab for deployment because it is the natural continuation of their DevOps journey.

Using the CI/CD pipeline, users can create their deployment jobs by writing their own custom `.gitlab-ci.yml` file or using a pre-defined [GitLab Template](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-templates). Users can also leverage GitLab provided containers to solve for specific deployment scenarios, such as [deploying to AWS](https://about.gitlab.com/blog/2020/12/15/deploy-aws/). 

Beyond using the generic pipeline, GitLab offers [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) that enables users to get up and running quickly and easily yet maintain control of each step of the DevOps process.

There are also additional ways to deploy using GitLab. For example, you can [deploy to an attached cluster](https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster). You can now also run pull-based GitOps using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/). Lastly, we are working to make it easier to quickly deploy a stateful app that runs on hypercloud via experimenting through the [5-minute production app](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/tree/master).

This represents at least 6 deployment-related flavors within GitLab.

1. Users create a custom `.gitlab-ci.yml` for their project.
1. Users use a [templated `.gitlab-ci.yml`](https://about.gitlab.com/blog/2020/09/23/get-started-ci-pipeline-templates/) for their project. Some templates include reference to specific [deploy images](https://about.gitlab.com/blog/2020/12/15/deploy-aws/) to facilitate things working out of the box.
1. Users can use [Auto Devops](https://docs.gitlab.com/ee/topics/autodevops/) to have the pipeline and various GitLab integrations working by default. 
1. Users can attach a [Kubernetes cluster to GitLab](https://docs.gitlab.com/ee/user/project/clusters/). Once done, [those clusters can be deployment targets](https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster) with special deployment variables.
1. Users can use the [Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/) and use a GitOps deployment workflow.
1. Users can potentially use [5-minute Production app](https://about.gitlab.com/direction/5-min-production/) to deploy their web apps quickly.

Given the myriad options that exist, we intend to consolidate.

Developers should still be able to create their custom or use templated `.gitlab-ci.yml` for deployments. We plan to make things fast so efforts such as the [5-minute production app](/direction/5-min-production/) are not separate efforts. Platform engineers should be able to define deployment pipeline at the group level, whether using Auto DevOps or not. Users should not attach Kubernetes clusters to GitLab, rather they should use the Kubernetes agent for k8s based deployments and general management. 

Despite the deployment options GitLab provides, we also acknowledge that there are instances where users will choose to integrate with another tool for deployment. If that is the case, we'd love to hear from you to understand why.

## Challenges

The [Ops section challenges](/direction/ops/#challenges) are applicable to deployment. There are some challenges that are worth expanding on with some additional highlights:

* CNCF/cloud-native open-source solutions can disrupt GitLab's one application vision. They are marketed to a huge and engaged audience. If they're successful in growing adoption, it introduces a barrier to adopting GitLab. For deployment, a risky and highly visible part of the SDLC process, organizations may be more reticent to switch to GitLab's one application solution.
* Our target customers are also our main competition as cloud platform teams often have a charter, or at least leeway, to build a DIY platform.
* GitLab's pipeline-based deployment solution targets the developer as the primary persona. As a deployment tool, it may be less effective relative to solutions that target the operator as the primary persona with specific tooling made for their primary jobs.

## Opportunities

The [Ops section opportunities](/direction/ops/#opportunities) apply to deployment. GitLab also has some important opportunities to take advantage of specific to deployment:

* **Deployments is a separate problem:** Deployment is not the same as [CD](https://martinfowler.com/bliki/ContinuousDelivery.html) and shouldn't be solved with the same tool as CD. Yet, because GitLab customers already use the CI/CD pipeline as a central component in their software development life cycle, moving downstream to solve deployment is a natural expansion for GitLab and its customers.
* **DevOps journey is stalled at Deployment:** [Delivering software quickly, reliably, and safely is at the heart of technology transformation and organizational performance, yet still only a small percentage (20%) are elite performers](https://services.google.com/fh/files/misc/state-of-devops-2019.pdf). Small organization lacks the resources to staff a platform team to create efficient deployment capabilities as they scale, large organizations are hindered by the complexity of deployment. Now is the perfect time to enable organizations to get good at deployments through a product.

## Key Capabilities for Deployment

Enterprises are increasingly choosing to have a [cloud-first strategy](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526573920). Furthermore, with the increasing adoption of microservices architecture and infrastructure as code, traditional release tools are inadequate. This, coupled with the traditional deployment requirement of governance, compliance, and security, means that deployment tools have to meet a high bar and address a set of evolving technical and compliance requirements. The primary themes for these capabilities are that first organizations need **collaboration and transparency**, then **control and compliance** and before requiring **measurement and advanced patterns**. We list these key capabilities of deployment platforms in **priority order of user need**:

1. **Environment management:** Organizations typically operate multiple environments, each serving different needs. Deployment tools should help to make managing environments easy and intuitive.
1. **Everything as code:** Deployment tooling, including pipelines, infrastructure, environments, and monitoring tools, are constantly evolving. If they can be stored as code and version controlled, it will enable organizations to more effectively collaborate and avoid costly mistakes.
1. **GitOps:** Simply checking code into a repository will not prevent drift to occur between code and what is deployed. [GitOps](https://about.gitlab.com/topics/gitops/) solves this problem by automatically managing changes using the single source of truth reflected in the source repository.
1. **Release Orchestration & Quality gates:** Organizations need to control what can be deployed and in which sequence. Enabling reviews and approvals built right into the deployment workflow is a critical requirement. The ability to perform automatic rollbacks, environment freeze, scaling deployment also enables organizations to be more in control.
1. **Feedback/Reporting:** Deployment is a critical part of the DevOps feedback loop. Successful deployment depends on both immediate feedback from Monitoring and Observability tools to ensure a health deploy as well medium term health of DevOps teams by measuring DORA4 and other success metrics.
2. **Progressive delivery:** Deployments can be risky. To mitigate risks, progressive delivery techniques, such as using feature flags, can help mitigate the risk by limiting the impact until the deployment teams are confident that their changes are good to go.

## Strategy

In Deployment, we are targeting [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer) as our primary persona. She is responsible for setting up the systems her company's development teams use to develop, test, **ship**, and **operate** their applications. It is likely she works at an enterprise where there is a mix of deployment targets. She also faces the challenges of ever-increasing complexity; as more contributors, more services, more infrastructure, more databases, and a mix of technologies are added to her applications. She is looking for ways to create a system to manage the complexity.

We plan to iteratively create a solution by focusing on two strategic vectors. We want to make it easy for Priyanka to:

1. **Enable Developers to deploy Kubernetes**. Kubernetes is growing fast and also is a shortcut around building specific integrations with cloud vendors. Kubernetes is an efficient target for deployment. It is also where deployment innovation is happening fastest. It is important that we [mitigate this low-end disruption](/direction/#mitigating-low-end-disruption) and make deployments to Kubernetes that work by default. 
1. **Meet Enterprises where they are at**. Enterprises need help managing deployment complexity. They need RBAC for deployment, visibility to what is deployed, and to where, the ability to orchestrate deployment activities, to fulfill compliance and security requirements, and to ensure development teams follow established best practices. There are many GitLab enterprise customers that have bought into GitLab's vision, yet they are blocked from adopting GitLab's release features because our solution is reliant on the CI/CD pipeline at the project level.

### One Year Plan

To execute on our strategy, we are creating two new categories:

1. **Environment Management** - Within GitLab today, Priyanka defines manages environments by defining them directly in `gitlab-ci.yaml`. This works well when she is managing environments per project, but not when she is managing central platform environments that have multiple applications deploying to them from separate projects. She needs visibility to the environments and the deployments to those environments from across multiple projects. 
1. **Platform Management** - Within GitLab today, Priyanka uses `gitlab-ci.yaml` templates and snippets to publish deployment best practices across projects to platforms. This works when the number of projects is small, and the infrastructure and deployment patterns are sufficiently common. This becomes painful when Priyanka is managing complex infrastructure with multiple deployment methods or when trying to understand the specific projects deploying to the platform. Priyanka needs a way to enable multiple teams to follow the best practices she defines across multiple projects. 

And continue our progress on current capabilities:
1. **Kubernetes Management** - We've recently adjusted our approach to connecting Kubernetes clusters to your GitLab instance (with the Kubernetes Agent) and we will add cluster management capabilities to GitLab to be successful here.
1. **Release Orchestration** - We'd previously made investments in improving the experience for more traditional release engineering processes. We are restarting this investment to provide a foundation for platform teams who need to participate in a centrally coordinated release effort. We will shift our focus to enable Priyanka to control access to specific environments, to set up approval workflows. We will also enable Priyanka with tools to set up release managers to organize releases from multiple projects that will be deployed together. 
1. **Infrastructure as Code** - We've made considerable improvements to the way you manage your infrastructure with Terraform from within GitLab. We will continue to build capabilities for sharing, collaborating, and enabling developer use of infrastructure definitions provided by platform teams.
1. **Measure Success** - GitLab is uniquely positioned as a solution to visualize how well teams are shipping software. By surfacing important health metrics, such as the DORA4 metrics, will help fuel improvement and adoption of additional GitLab features.

### What we aren't focused on now
There are important things we won't work on.

1. **Auto DevOps Deployments** - While we will enable the creation of Auto DevOps style templates and experiences for the developers in their organization by platform teams, we will not be making significant strides to make Auto DevOps deployments cover a broader set of use cases at this time.
1. **Progressive Delivery** - By focusing on where platform teams are today, we'll forgoe pushing further on our current progressive delivery capabilities like Feature Flags, A/B Testing and Canary Deployments. 
1. **Cost Optimization** - We should first up our Kubernetes Management capability before focusing more on cluster costs. Enterprises want views into costs beyond clusters. Building capabilities like environment management precedes cost optimization tooling.
1. **Non-Kubernetes Cloud-native** - Distinguishing from [Kubernetes-native](https://cloudark.medium.com/towards-a-kubernetes-native-future-3e75d7eb9d42), which is our initial focus area. We will not be focused on other cloud-native solutions, such as Docker Swarm, Apache Mesos, and Amazon ECS, because they're not nearly as successful as kubernetes.
1. **Building more Deployment Methods** - Actively adding templates or improving existing templates is not our main focus. Nor is building customized images for deploying to cloud providers. The CI/CD pipeline is flexible and enables GitLab users to create what they need as is. It is worthwhile to examine how we can enable the wider GitLab community, including our customer success teams, to share and reuse similar templates and treat them as lego blocks that can be adopted and put to use quickly. These will be most beneficial for common deployment targets, such as FarGate and Lambda.
1. **[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)** - While the deployment of static sites is a large market, GitLab Pages is more of a fit for sharing static webpages within an organization today. We are exploring how to accelerate deploying static content more generally in the [Jamstack SEG](https://about.gitlab.com/handbook/engineering/incubation/jamstack/) 

### What's next

- Release Group - [What's Next for CD](https://about.gitlab.com/direction/release/continuous_delivery/#whats-next--why)
- Configure Group - [What's Next](https://about.gitlab.com/direction/configure/#opportunities)

Beyond Release and Configure, Monitor is also an important aspect of operating your production deployments. We will be adding direction content on this topic.

Lastly, within GitLab, the nomenclature for deployments, environments, release, delivery are often inconsistent, interchangeable, and confusing. We need to settle on a standard and build it into our products and documentation.

## Jobs To Be done

To be added

## Competitive Landscape

<%= partial("direction/ops/competition/github-actions") %>

<%= partial("direction/ops/competition/harness") %>

<%= partial("direction/ops/competition/spinnaker") %>

<%= partial("direction/ops/competition/waypoint") %>

<%= partial("direction/ops/competition/argo-cd") %>

<%= partial("direction/ops/competition/weaveworks") %>

<%= partial("direction/ops/competition/jenkins-x") %>

<%= partial("direction/ops/competition/octopus-deploy") %>

<%= partial("direction/ops/competition/cloudbees-electric-flow") %>

<%= partial("direction/ops/competition/IBM-urbancode") %>

<%= partial("direction/ops/competition/digitalai") %>

